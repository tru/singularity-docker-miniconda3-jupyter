From continuumio/miniconda3
# update conda and the OS
RUN	conda update conda && \
	conda upgrade --all -y && \
	apt-get update && \
	DEBIAN_FRONTEND=noninteractive apt-get -y upgrade && \
	DEBIAN_FRONTEND=noninteractive apt-get -y autoremove && \
	DEBIAN_FRONTEND=noninteractive apt-get -y clean autoclean

RUN	conda install ipykernel jupyterlab
# download the yml file
#RUN curl  https://gitlab.pasteur.fr/tru/conda-container-howto/-/raw/main/pythonnet.yml > pythonnet.yml
#RUN conda env create --file pythonnet.yml
