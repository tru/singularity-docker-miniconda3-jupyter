## installing a jupyter minimal environment in singularityCE/apptainer and docker

Merging https://gitlab.pasteur.fr/tru/singularity-miniconda3-jupyter and https://gitlab.pasteur.fr/tru-docker-miniconda3-jupyter
with 2 parallel CI builds.

Attn: `:main` is the docker image, `:latest` is the OCI container 

## singularity (singularityCE or apptainer installed)
```
singularity build miniconda3-jupyter.sif  oras://registry-gitlab.pasteur.fr/tru/singularity-docker-miniconda3-jupyter:latest
singularity exec miniconda3-jupyter.sif jupyter-lab  --no-browser
```

## on Maestro.pasteur.fr
```
module add apptainer
apptainer build miniconda3-jupyter.sif  oras://registry-gitlab.pasteur.fr/tru/singularity-docker-miniconda3-jupyter:latest
apptainer exec -B /pasteur miniconda3-jupyter.sif jupyter-lab  --no-browser
```

## from cache only
```
singularity exec oras://registry-gitlab.pasteur.fr/tru/singularity-docker-miniconda3-jupyter:latest jupyter-lab  --no-browser
```

## docker image 
```
docker run -ti docker://registry-gitlab.pasteur.fr/tru/singularity-docker-miniconda3-jupyter:main
```
